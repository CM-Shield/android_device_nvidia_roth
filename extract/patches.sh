# Copyright (C) 2021 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Only the molly nvos works with t114 libraries on N or newer
function fetch_molly_nvos() {
  mkdir -p ${LINEAGE_ROOT}/${OUTDIR}/t114/common/lib
  wget 'https://dumps.tadiphone.dev/dumps/google/molly/-/raw/molly-user-5.0.2-LRX22G-1649326-release-keys/system/lib/libnvos.so' -O ${LINEAGE_ROOT}/${OUTDIR}/t114/common/lib/libnvos.so
}

fetch_molly_nvos;
