#
# Copyright (C) 2018 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

BOARD_FLASH_BLOCK_SIZE             := 4096
BOARD_BOOTIMAGE_PARTITION_SIZE     := 8388608
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 8388608
BOARD_CACHEIMAGE_PARTITION_SIZE    := 805306368
BOARD_USERDATAIMAGE_PARTITION_SIZE := 13771997184
BOARD_SYSTEMIMAGE_PARTITION_SIZE   := 805306368
TARGET_USERIMAGES_USE_EXT4         := true

# Assert
TARGET_OTA_ASSERT_DEVICE := roth

# Bluetooth
ifeq ($(TARGET_TEGRA_BT),bcm)
BOARD_CUSTOM_BT_CONFIG  := device/nvidia/roth/comms/vnd_roth.txt
BCM_BLUETOOTH_MANTA_BUG := true
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := device/nvidia/roth/comms
endif

# Bootloader versions
TARGET_BOARD_INFO_FILE := device/nvidia/roth/board-info.txt

# Hardware overlays
BOARD_HARDWARE_CLASS := device/nvidia/roth/lineagehw/

# Kernel
ifneq ($(TARGET_PREBUILT_KERNEL),)
BOARD_VENDOR_KERNEL_MODULES += $(wildcard $(dir $(TARGET_PREBUILT_KERNEL))/*.ko)
else ifeq ($(TARGET_TEGRA_KERNEL),34)
TARGET_KERNEL_SOURCE    := kernel/nvidia/roth
TARGET_KERNEL_CONFIG    := lineageos_roth_defconfig
BOARD_KERNEL_IMAGE_NAME := zImage
else
TARGET_KERNEL_SOURCE    := kernel/nvidia/linux-5.4
TARGET_KERNEL_CONFIG    := tegra_android_defconfig
BOARD_KERNEL_IMAGE_NAME := zImage
endif
LZMA_RAMDISK_TARGETS    := recovery

# Manifest
DEVICE_MANIFEST_FILE := device/nvidia/roth/manifest.xml

# Recovery
TARGET_RECOVERY_FSTAB := device/nvidia/roth/initfiles/fstab.roth

# Sensors
TARGET_SENSOR_VARIANT := tegra

# TWRP Support
ifeq ($(WITH_TWRP),true)
include device/nvidia/roth/twrp/twrp.mk
endif

include device/nvidia/t114-common/BoardConfigCommon.mk
include device/nvidia/touch/BoardConfigTouch.mk
