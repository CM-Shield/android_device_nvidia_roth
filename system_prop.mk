ifneq ($(TARGET_TEGRA_KERNEL),34)
# USB configfs
PRODUCT_PROPERTY_OVERRIDES += \
    sys.usb.udc=7d000000.udc \
    sys.usb.controller=7d000000.udc
endif
